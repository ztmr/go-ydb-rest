package main

// TODO:
//   - support for YDB TP
//   - docker file
//   - return json success/error status back to the client along with the reply
//   - graphql
//   - ydb signal handling
//   - opentracing (jaeger)
//   - log requests/responses when debug mode enabled

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/oklog/ulid"
	"lang.yottadb.com/go/yottadb"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

// The person Type (more like an object)
type Person struct {
	ID              string   `json:"id,omitempty"`
	Firstname       string   `json:"firstname,omitempty"       ydb:"key=^Person(:ID);piece=1"`
	Lastname        string   `json:"lastname,omitempty"        ydb:"key=^Person(:ID);piece=2"`
	Address         *Address `json:"address,omitempty"         ydb:"key=^Person(:ID);piece=3"`
	PersistentValue string   `json:"persistentValue,omitempty" ydb:"key=^Person(:ID,'%PV');piece=1"`
}
type Address struct {
	ID    string `json:"id,omitempty"`
	City  string `json:"city,omitempty"  ydb:"key=^Address(:ID);piece=1"`
	State string `json:"state,omitempty" ydb:"key=^Address(:ID);piece=2"`
}

/*
type Story struct {
	StorySnapshot
}

type StorySnapshot struct {
	ID         string
	Group      string
	Origin     string
	Released   string
	Created    string
	Author     string
}
*/

const GBL_GVN_PERSON string = "^Person"

const YDB_DEL_TREE int = 1

func GenerateID() string {
	t := time.Now()
	entropy := ulid.Monotonic(rand.New(rand.NewSource(t.UnixNano())), 0)
	return ulid.MustNew(ulid.Timestamp(t), entropy).String()
}

func GetPeople(w http.ResponseWriter, r *http.Request) {
	var people []Person
	var startID = r.URL.Query().Get("startID")
	batchSize, errConv := strconv.Atoi(r.URL.Query().Get("batchSize"))
	if errConv != nil {
		batchSize = 20
	}
	people, err := (Person{}).Order(startID, batchSize, nil)
	if err != nil {
		log.Print("Person Order Error: ", err)
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		json.NewEncoder(w).Encode(people)
	}
}

func GetPerson(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var person Person
	person, err := person.Load(params["id"])
	if err == nil {
		json.NewEncoder(w).Encode(person)
	} else {
		log.Print(err)
		w.WriteHeader(http.StatusNotFound)
	}
}

func CreatePerson(w http.ResponseWriter, r *http.Request) {
	var person Person
	_ = json.NewDecoder(r.Body).Decode(&person)
	person, err := person.Save()
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// NOTE: get what we have really stored
	personNew, errNew := person.Load(person.ID)
	if errNew == nil {
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(personNew)
	} else {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func UpdatePerson(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var person Person
	personOrig, errOrig := person.Load(params["id"])
	if errOrig != nil {
		log.Print(errOrig)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	_ = json.NewDecoder(r.Body).Decode(&personOrig)
	if personOrig.ID != params["id"] {
		log.Print("Attempting to change ID field!")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	person, err := personOrig.Save()
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	personNew, errNew := person.Load(personOrig.ID)
	if errNew == nil {
		json.NewEncoder(w).Encode(personNew)
	} else {
		// XXX: nothing updated, the original record does not exist, this is really an assertion issue
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func DeletePerson(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var person Person
	person, err := person.Load(params["id"])
	if err == nil {
		person.Delete()
		json.NewEncoder(w).Encode(person)
	} else {
		log.Print(err)
		w.WriteHeader(http.StatusNotFound)
	}
}

func HandleError(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusBadRequest)
}

func AppShutdown(w http.ResponseWriter, r *http.Request) {
	// XXX: this should be done using basic auth similar to
	// this one:
	//   https://gist.github.com/sambengtson/bc9f76331065f09e953f
	var sysMgrToken string = r.Header.Get("X-SysMgr-Token")
	if sysMgrToken == "sysMgrSecretToken" {
		w.WriteHeader(http.StatusAccepted)
		go func() {
			log.Printf("Graceful shutdown requested by sysmgr...")
			time.Sleep(2 * time.Second)
			os.Exit(0)
		}()
		return
	} else {
		w.WriteHeader(http.StatusUnauthorized)
	}
}

func (p Person) Exists(ID string) bool {
	var dval uint32
	var err error
	dval, err = yottadb.DataE(yottadb.NOTTP, GBL_GVN_PERSON, []string{ID})
	return (err == nil) && (0 != int(dval))
}

// ODB tests
func (p Person) Load(ID string) (Person, error) {
	var val string
	if !p.Exists(ID) {
		return p, errors.New("Record with this id was not found")
	}
	val, err := yottadb.ValE(yottadb.NOTTP, GBL_GVN_PERSON, []string{ID})
	if err != nil {
		return p, err
	}
	var b bytes.Buffer
	b.WriteString(val)
	d := gob.NewDecoder(&b)
	if err := d.Decode(&p); err != nil {
		return p, err
	}
	p.ID = ID
	return p, nil
}

func (p Person) Save() (Person, error) {
	if p.ID == "" {
		p.ID = GenerateID()
	}
	var b bytes.Buffer
	e := gob.NewEncoder(&b)
	if err := e.Encode(p); err != nil {
		return p, err
	}
	if err := yottadb.SetValE(yottadb.NOTTP, b.String(), GBL_GVN_PERSON, []string{p.ID}); err != nil {
		return p, err
	} else {
		return p, nil
	}
}

type OrderFilter func(Person) bool

func (Person) Order(startID string, batchSize int, filter OrderFilter) ([]Person, error) {
	var tptoken uint64
	tptoken = yottadb.NOTTP
	var people []Person
	var person Person
	var err error
	var cur_sub = startID
	if filter == nil {
		filter = func(p Person) bool { return true }
	}
	for i := 0; i < batchSize; {
		cur_sub, err = yottadb.SubNextE(tptoken, GBL_GVN_PERSON, []string{cur_sub})
		if err != nil {
			error_code := yottadb.ErrorCode(err)
			if error_code == yottadb.YDB_ERR_NODEEND {
				break
			} else {
				return nil, err
			}
		}
		person, err := person.Load(cur_sub)
		if err != nil {
			log.Print("Person Load Error: ", err)
		} else {
			if filter(person) {
				people = append(people, person)
				i++
			}
		}
	}
	return people, nil
}

func (p Person) Delete() error {
	if p.ID == "" {
		return errors.New("Cannot delete object without ID!")
	}
	return yottadb.DeleteE(yottadb.NOTTP, YDB_DEL_TREE, GBL_GVN_PERSON, []string{p.ID})
}

func main() {
	var port int
	flag.IntVar(&port, "port", 8080, "port number where to listen")
	flag.Parse()

	router := mux.NewRouter()
	router.HandleFunc("/v1/person", GetPeople).Methods("GET")
	router.HandleFunc("/v1/person", CreatePerson).Methods("PUT")
	router.HandleFunc("/v1/person/{id}", GetPerson).Methods("GET")
	router.HandleFunc("/v1/person/{id}", UpdatePerson).Methods("POST")
	router.HandleFunc("/v1/person/{id}", DeletePerson).Methods("DELETE")
	router.HandleFunc("/v1/sysmgr/shutdown", AppShutdown).Methods("POST")
	router.HandleFunc("/", HandleError)

	var gracefulStop = make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	signal.Notify(gracefulStop, syscall.SIGUSR2)
	go func() {
		sig := <-gracefulStop
		log.Printf("\n *** Caught sig: %+v", sig)
		log.Printf("Waiting for 2 second to shutdown gracefuly...")
		time.Sleep(2 * time.Second)
		os.Exit(0)
	}()

	log.Printf("Starting up the service at port :%d", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), router))
}
